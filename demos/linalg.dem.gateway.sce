// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("linalg.dem.gateway.sce");
subdemolist = [
//"rayleighquotient", "rayleighquotient.sce"; ..
//"linalg_zhbev", "linalg_zhbev.sce"; ..
//"linalg_zgemm", "linalg_zgemm.sce"; ..
//"linalg_solvelu", "linalg_solvelu.sce"; ..
//"linalg_hbandL", "linalg_hbandL.sce"; ..
//"linalg_gausspivotalnaive", "linalg_gausspivotalnaive.sce"; ..
//"linalg_gausspivotal", "linalg_gausspivotal.sce"; ..
//"linalg_gaussnaive", "linalg_gaussnaive.sce"; ..
//"linalg_factorlupivot", "linalg_factorlupivot.sce"; ..
//"linalg_factorlu", "linalg_factorlu.sce"; ..
//"linalg_dsyev", "linalg_dsyev.sce"; ..
//"linalg_dgemm", "linalg_dgemm.sce"; ..
"exercise_power", "exercise_power.sce"; ..
"exercise_chol", "exercise_chol.sce"; ..
"demoexpm", "demoexpm.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
