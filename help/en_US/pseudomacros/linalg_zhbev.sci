// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function D = linalg_zhbev ( AHLband )
  // Computes the eigenvalues of a complex hermitian band matrix.
  //
  // Calling Sequence
  //   D = linalg_zhbev ( AHLband )
  //
  // Parameters
  // AHLband : a mb-by-nb matrix of complex doubles
  // D : a n-by-1 matrix of doubles
  //
  // Description
  //   Computes the eigenvalues of a complex hermitian band matrix by calling LAPACK/ZHBEV.
  //
  //   Expects AHLband to be stored in Hermitian Band Lower Storage format.
  //   An m-by-n complex hermitian band matrix with kd subdiagonals and kd superdiagonals
  //   may be stored compactly in a two-dimensional array with mb=kd+1 rows
  //   and nb=n columns.
  //   Columns of the matrix are stored in corresponding columns of the array,
  //   and diagonals of the matrix are stored in rows of the array.
  //   This storage scheme should be used in practice only if
  //
  //   <latex>
  //   \begin{eqnarray}
  //   kd \ll \min(m,n),
  //   \end{eqnarray}
  //   </latex>
  //
  //   although LAPACK routines work correctly for all values of kd.
  //   In LAPACK, arrays that hold matrices in band storage have names ending in "B".
  //   We store in AHLband only the lower triangular part of the full matrix A.
  //   To be precise, A(i,j) is stored in AHLband(1+i-j,j) for
  //
  //   <latex>
  //   \begin{eqnarray}
  //   j \leq i \leq \min(n,j+kd).
  //   \end{eqnarray}
  //   </latex>
  //
  //   We use the UPLO="L" option of the ZHBEV routine.
  //   In order to convert a full matrix into the Hermitian Band Lower storage form, we
  //   may use the linalg_hbandL function.
  //
  //   See the Lapack User's Guide, section "Matrix Storage Schemes"/"Band storage" for details.
  //
  // The linalg_zhbev function provides the same feature as Lapack's
  // ZHBEV, that is, computes the eigenvalues of a complex hermitian band matrix.
  // The other method in Scilab to compute eigenvalues of sparse matrices
  // is to use Arnoldi's iterations.
  // This does not make use of the Hermitian structure of some matrices.
  // This is why the linalg_zhbev is much faster than Scilab's routines for
  // Hermitian band matrices.
  //
  // Examples
  // AHLband = [
  //   1 2 3;
  //   4 5 0;
  //   6 0 0
  // ];
  // AHLband = complex(AHLband,0);
  // computed = linalg_zhbev(AHLband)
  // // With 6 significant digits :
  // expected = [-4.094602,-2.0337914,12.128393]'
  // // Compare with ScilAHLband's:
  // A = [
  // 1 4 6
  // 4 2 5
  // 6 5 3
  // ];
  // A = complex(A,0);
  // expected = spec(A);
  // expected = gsort(expected,"g","i")
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2010 - Sylvan Brocard
  //
  // Bibliography
  //  Lapack User's Guide, Band storage, http://www.netlib.org/lapack/lug/node124.html
  //  http://www.equalis.com/forums/posts.asp?group=&topic=144288&DGPCrPg=1&hhSearchTerms=&#Post158559
  //

endfunction


