// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function D = linalg_dgemm ( Alpha , A , B , Beta , C)
  // Computes D = Alpha*A*B+ Beta*C for a real matrix.
  //
  // Calling Sequence
  //   D = linalg_dgemm ( Alpha , A , B , Beta , C)
  //
  // Parameters
  // Alpha : a 1-by-1 matrix of doubles (real)
  // A : a m-by-n matrix of doubles (real)
  // B : a n-by-p matrix of doubles (real)
  // Beta : a 1-by-1 matrix of doubles (real)
  // C : a m-by-p matrix of doubles (real)
  // D : a m-by-p matrix of doubles (real), D = Alpha*A*B+ Beta*C
  //
  // Description
  //   Calls BLAS/DGEMM.
  //
  // Examples
  // Alpha = 2;
  // Beta = 3;
  // m = 3;
  // n = 4;
  // C = ones(m, n);
  // k = 2;
  // A = ones(m, k);
  // B = ones(k, n);
  // D = linalg_dgemm(Alpha, A, B, Beta, C)
  // // Compare with Scilab's:
  // Alpha*A*B+ Beta*C
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

endfunction


