// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function X = linalg_zgesv ( A , B )
    // Solves A*X = B for complex equations.
    //
    // Calling Sequence
    //   X = linalg_dgesv ( A , B )
    //
    // Parameters
    // A : a n-by-n matrix of doubles (complex)
    // B : a n-by-p matrix of doubles (complex)
    // X : a n-by-p matrix of doubles (complex), the solution of A*X = B.
    //
    // Description
    //   Calls BLAS/ZGESV.
    //
    // Examples
    // // One RHS in the linear system of equations
    // A=  [
    // 1 2
    // 3 4
    // ] + %i* [
    // 5 6
    // 7 8];
    // e = [5;6] + %i*[7;8];
    // b = [-66;-74] + %i * [84;136];
    // x = linalg_zgesv(A,b);
    // // 3 RHS in the linear system of equations
    // A=  [
    // 1 2
    // 3 4
    // ] + %i* [
    // 5 6
    // 7 8];
    // e = [5,1,2,3;6,2,3,4]+%i*[7,8,9,10;8,9,10,11];
    // b = [-66,-89,-97,-105;-74,-117,-125,-133] + %i*[84,43,57,71;136,83,105,127];
    // x = linalg_zgesv(A,b);
    // // A singular system
    // A=  [
    // 1 2
    // 1 2
    // ] + %i* [
    // 5 6
    // 5 6];
    // b = [-66;-74] + %i * [84;136];
    // x = linalg_zgesv(A,b)
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //

endfunction


