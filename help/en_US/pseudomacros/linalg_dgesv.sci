// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function X = linalg_dgesv ( A , B )
    // Solves A*X = B for real equations.
    //
    // Calling Sequence
    //   X = linalg_dgesv ( A , B )
    //
    // Parameters
    // A : a n-by-n matrix of doubles (real)
    // B : a n-by-p matrix of doubles (real)
    // X : a n-by-p matrix of doubles (real), the solution of A*X = B.
    //
    // Description
    //   Calls BLAS/DGESV.
    //
    // Examples
    // // One RHS in the linear system of equations
    // A=  [
    // 1 2
    // 3 4
    // ];
    // e = [5;6];
    // b = [17;39];
    // x = linalg_dgesv(A,b);
    // // 3 RHS in the linear system of equations
    // A=  [
    // 1 2
    // 3 4
    // ];
    // e = [5,1,2,3;6,2,3,4];
    // b = [17,5,8,11;39,11,18,25];
    // x = linalg_dgesv(A,b);
    // // A singular system
    // A=  [
    // 1 2
    // 1 2
    // ];
    // b = [-66;-74];
    // x = linalg_dgesv(A,b)
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //

endfunction


