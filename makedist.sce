//  Copyright (C) 2023 Stéphane Mottelet.
//
//  SCIIPOPT is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the
//  Free Software Foundation; either version 2, or (at your option) any
//  later version.
//
//  This part of code is distributed with the FURTHER condition that it 
//  can be compiled and linked with the Scilab libraries and it can be 
//  used within the Scilab environment.
//
//  SCIIPOPT is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
//  for more details.

path_makedist = get_absolute_file_path("makedist.sce");
cd(path_makedist);
[_,toolboxname] = fileparts(pwd());
version = mgetl("VERSION");

deletefile(".gitignore");
[sci,v]=getversion()
http_get("https://atoms.scilab.org/toolboxes/"+toolboxname+"/"+version+"/DESCRIPTION","DESCRIPTION")
if getos() <> "Windows" then
    filename = toolboxname+"_"+version+".bin."+v(2)+"."+getos()+".tar.gz"
    cd ..
    movefile(toolboxname+"/makedist.sce","makedist.sce")
    unix_g("tar cvzf "+filename+" "+toolboxname)
else
    filename = toolboxname+"_"+version+".bin."+v(2)+"."+getos()+".zip"
    cd ..
    movefile(toolboxname+"/makedist.sce","makedist.sce")
    host(SCI+"/tools/zip/zip.exe -r "+filename+" "+toolboxname)
end
movefile("makedist.sce",toolboxname+"/makedist.sce")
cd(path_makedist);
