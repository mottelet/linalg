// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function Afull = linalg_hfullL(AHLband)
    // Converts a complex hermitian band matrix in its compact form
    // into its full form.
    //
    // Calling Sequence
    //   Afull = linalg_hfullL(AHLband)
    //
    // Parameters
    // AHLband : a ndiags-by-n matrix of doubles, the Hermitian Band Lower
    //           storage format of Afull.
    // Afull : a n-by-n hermitian matrix of complex doubles.
    //
    // Description
    // Transforms a complex Hermitian Band matrix from its compact form
    // used for the zhbev algorithm into its full form,
    // for comparison purposes.
    // AB is the lower triangular part of the full matrix A.
    //
    // Examples
    // AHLband = [3       4      -12
    //            1-%i*7 -2+%i*8   0];
    // Afull = linalg_hfullL(AHLband);
    // expected = [3       1+7*%i   0
    //             1-7*%i  4       -2-8*%i
    //             0      -2+8*%i -12];
    //
    // // Combine hfullL and zhbev
    // // Create a Hermitian Band Lower matrix.
    //   n = 50;
    //   nsubdiag = 4;
    //   Ar = zeros(nsubdiag+1, n);
    //   for i = 1 : n
    //       j = (i:i+nsubdiag)';
    //       j = max(min(j, n), 1);
    //       Ar(j-i+1, i) = j;
    //   end
    //   Ai = zeros(nsubdiag+1, n);
    //   for i = 1 : n-1
    //       j = (i+1:i+nsubdiag)';
    //       j = max(min(j, n), 1);
    //       Ai(j-i+1, i) = j;
    //   end
    //   AHLband = (Ar+%i*Ai);
    // // Convert it into full form
    // Afull = linalg_hfullL(AHLband);
    // // Use zhbev
    // D = linalg_zhbev(AHLband);
    // D = gsort(D, "g", "i");
    // // Compare with spec
    // Dfull = spec(Afull);
    // Dfull = gsort(Dfull, "g", "i");
    // // Display relative error : rtol should be from 10 to 1000
    // rtol = max(abs(D-Dfull) ./ abs(D)/%eps)
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 2010 - Sylvan Brocard

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_hfullL", rhs, 1);
    apifun_checklhs("linalg_hfullL", lhs, 0:1);

    // Check type
    apifun_checktype("linalg_hbandL", AHLband, "AHLband", 1, "constant");

    // Proceed...
    [ndiags, nmax] = size(AHLband);
    Afull = zeros(nmax, nmax);
    isHermitian = %T;
    for i = 1:nmax
        isHermitian = imag(AHLband(1, i)) < %eps;
        if ~isHermitian then
            complexterm = i;
            break
        end
    end
    if ~isHermitian then
        localstr = gettext("%s: Incorrect input argument #%d. " ..
                         + "Expected a hermitian matrix, " ..
                         + "but %d-th diagonal element is complex.");
        msg = msprintf(localstr, "linalg_hfullL", 1, complexterm);
        warning(msg);
    end
    for j = 1:nmax
        m = min(nmax, j+ndiags-1);
        Afull(j, j) = real(AHLband(1, j));
        for i = j+1:m
            Afull(i, j) = AHLband(1+i-j, j);
            Afull(j, i) = AHLband(1+i-j, j)';
        end
    end
endfunction

