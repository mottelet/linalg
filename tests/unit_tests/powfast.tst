// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->



A = [
    5.    4.    3.    2.    1.
    4.    4.    3.    2.    1.
    0.    3.    3.    2.    1.
    0.    0.    2.    2.    1.
    0.    0.    0.    1.    1.
];
//
B = linalg_powfast(A,5);
E = [
    37721.    47455.    45750.    34540.    18160.
    33940.    42751.    41245.    31150.    16380.
    15420.    19695.    19156.    14525.    7650.
    3720.     5010.     5020.     3861.     2045.
    360.      570.      620.      495.      266.
];
assert_checkequal(B,E);

//
B = linalg_powfast(A,6);
E = [
    378425.    477954.    461858.    349092.    183626.
    340704.    430499.    416108.    314552.    165466.
    155880.    197928.    191863.    145242.    76446.
    38640.     49980.     48972.     37267.     19656.
    4080.      5580.      5640.      4356.      2311.
];
assert_checkequal(B,E);

//
B = linalg_powfast(A,7);
E = [
    3803941.    4811090.    4652895.    3518284.    1850955.
    3425516.    4333136.    4191037.    3169192.    1667329.
    1571112.    1990821.    1927497.    1458272.    767359.
    393120.     501396.     487310.     369374.     194515.
    42720.      55560.      54612.      41623.      21967.
];
assert_checkequal(B,E);


