// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



Alpha = 2;
Beta = 3;
m = 3;
n = 4;
C = ones(m, n);
k = 2;
A = ones(m, k);
B = ones(k, n);
computed = linalg_dgemm(Alpha, A, B, Beta, C);
// D = Alpha*A*B+ Beta*C
expected = 7*ones(m,n);
assert_checkalmostequal ( computed , expected , %eps );
//
Alpha = 0.1234;
Beta = 5.678;
m = 3;
n = 4;
k = 2;
C = matrix(1:m*n,m,n);
A = matrix(1:m*k,m,k);
B = matrix(1:k*n,k,n);
computed = linalg_dgemm(Alpha, A, B, Beta, C);
// D = Alpha*A*B+ Beta*C
expected = [
    6.7886     25.0566    43.3246    61.5926
    12.8368    31.5984    50.36      69.1216
    18.885     38.1402    57.3954    76.6506
];
assert_checkalmostequal ( computed , expected , [],[],"element");

//
// Check error
// matrix Alpha is complex and this does not work!
Alpha = 1 + %i;
Beta = Alpha;
A = matrix(1:4,2,2);
B = A;
C = A;
lclmsg = msprintf(_("%s: Wrong type for argument %d: Real matrix expected."),"linalg_dgemm",1);
instr = "linalg_dgemm(Alpha, A, B, Beta, C)";
assert_checkerror( instr , lclmsg );

//
// Check error
// matrix A is complex and this does not work!
Alpha = 11;
Beta = Alpha;
A = matrix(1:4,2,2) + %i * matrix(5:8,2,2);
B = A;
C = A;
instr = "computed = linalg_dgemm(Alpha, A, B, Beta, C)";
lclmsg = msprintf(_("%s: Wrong type for argument %d: Real matrix expected."),"linalg_dgemm",2);
assert_checkerror( instr , lclmsg );

