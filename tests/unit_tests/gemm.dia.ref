// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
Alpha = 2;
Beta = 3;
m = 3;
n = 4;
C = ones(m, n);
k = 2;
A = ones(m, k);
B = ones(k, n);
computed = linalg_gemm(Alpha, A, B, Beta, C);
// D = Alpha*A*B+ Beta*C
expected = 7*ones(m,n);
assert_checkalmostequal ( computed , expected , %eps );
//
Alpha = 0.1234;
Beta = 5.678;
m = 3;
n = 4;
k = 2;
C = matrix(1:m*n,m,n);
A = matrix(1:m*k,m,k);
B = matrix(1:k*n,k,n);
computed = linalg_gemm(Alpha, A, B, Beta, C);
// D = Alpha*A*B+ Beta*C
expected = [
    6.7886     25.0566    43.3246    61.5926
    12.8368    31.5984    50.36      69.1216
    18.885     38.1402    57.3954    76.6506
];
assert_checkalmostequal ( computed , expected , [],[],"element");
//
// matrix Alpha is complex and this DOES work!
Alpha = 1 + %i;
Beta = Alpha;
A = matrix(1:4,2,2);
B = A;
C = A;
computed = linalg_gemm(Alpha, A, B, Beta, C);
expected = [ 8+%i*8, 18+%i*18;
 12+%i*12, 26+%i*26];
assert_checkalmostequal ( computed , expected , 1.e-6 );
//
// matrix A is complex and this DOES work
Alpha = 11;
Beta = Alpha;
A = matrix(1:4,2,2) + %i * matrix(5:8,2,2);
B = A;
C = A;
computed = linalg_gemm(Alpha, A, B, Beta, C);
expected = [-649+%i*517,-803+%i*891;
-726+%i*682,-880+%i*1144];
assert_checkalmostequal ( computed , expected , 1.e-6 );
//
Alpha = 1 + %i;
Beta = Alpha;
A = ones(2,2) + %i * ones(2,2);
B = A;
C = A;
computed = linalg_gemm(Alpha, A, B, Beta, C);
// Alpha*A*B+ Beta*C
expected = [
  -4+6*%i  -4+ 6*%i
  -4+6*%i  -4+ 6*%i
];
assert_checkalmostequal ( computed , expected , %eps );
//
Alpha = 1.234 + %i*5.678;
Beta = -9.012 - %i*3.567;
m = 3;
n = 4;
k = 2;
C = matrix(1:m*n,m,n)+%i*matrix(10+(1:m*n),m,n);
A = matrix(1:m*k,m,k)+%i*matrix(-5+(1:m*k),m,k);
B = matrix(1:k*n,k,n)+%i*matrix(-9+(1:k*n),k,n);
computed = linalg_gemm(Alpha, A, B, Beta, C);
// Alpha*A*B+ Beta*C
expected = [
    231.681-324.867*%i    240.026-249.044*%i    248.371-173.221*%i    256.716-97.398*%i
    316.584-250.05*%i     279.505-164.355*%i    242.426-78.66*%i      205.347+7.035*%i
    401.487-175.233*%i    318.984-79.666*%i     236.481+15.901*%i     153.978+111.468*%i
];
assert_checkalmostequal ( computed , expected);
