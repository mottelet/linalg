// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

gateway_path = get_absolute_file_path("builder_gateway_c.sce");

libname = "linalggateway";
namelist = ["linalg_zhbev"   "sci_linalg_zhbev"
            "linalg_dgemm"   "sci_linalg_dgemm"
            "linalg_zgemm"   "sci_linalg_zgemm"
            "linalg_dsyev"   "sci_linalg_dsyev"
            "linalg_powfast" "sci_linalg_powfast"
            "linalg_dgesv"   "sci_linalg_dgesv"
            "linalg_zgesv"   "sci_linalg_zgesv"];
files = ["sci_linalg_zhbev.c"
         "sci_linalg_dgemm.c"
         "sci_linalg_zgemm.c"
         "sci_linalg_dsyev.c"
         "sci_linalg_dgesv.c"
         "sci_linalg_zgesv.c"
         "sci_linalg_powfast.c"
         "gw_linalg_support.c"];

ldflags = "";

if getos() == "Windows" then
    include2 = "..\..\src\c";
    include3 = SCI + "/modules/output_stream/includes";
    cflags = "-DWIN32 " ..
           + "-I""" + include2 + """ " ..
           + "-I""" + include3 + """";
else
    include1 = gateway_path;
    include2 = gateway_path + "../../src/c";
    include3 = SCI + "/../../include/scilab/localization";
    include4 = SCI + "/../../include/scilab/output_stream";
    include5 = SCI + "/../../include/scilab/core";
    cflags = "-I""" + include1 + """ " ..
           + "-I""" + include2 + """ " ..
           + "-I""" + include3 + """ " ..
           + "-I""" + include4 + """ " ..
           + "-I""" + include5 + """";
end

libs = ["../../src/c/liblinalg"];

tbx_build_gateway(libname, namelist, files, gateway_path, ..
                  libs, ldflags, cflags);

clear tbx_build_gateway;

