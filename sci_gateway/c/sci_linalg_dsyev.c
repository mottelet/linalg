// Copyright (C) INRIA
// Copyright (C) 2010 - DIGITEO - Allan CORNET
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <string.h>
#include <stdio.h>
// Headers from Scilab:
#include "sciprint.h"
#include "Scierror.h"
#include "machine.h"
#include "core_math.h"
#include "api_scilab.h"
#include "localization.h"
// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/* SUBROUTINE DSYEV(JOBZ, UPLO, N, A, LDA, W, WORK, LWORK, INFO) */

/***********************************/
/*                                 */
/*     [R d] = linalg_dsyev(A)     */
/*        d  = linalg_dsyev(A)     */
/*                                 */
/***********************************/

int sci_linalg_dsyev(GW_PARAMETERS)
{
    int M = 0;
    int N = 0;
    int NLHS = 0;
    int un = 1;
    int WORK = 0;
    int LWORKMIN = 0;
    int LWORK = 0;
    int INFO = 0;
    int LDA = 0;

    int * piAddr = NULL;
    SciErr sciErr;
    int iRet = 0;
    double * lrA;
    double * lrd;
    double * lrWORK;

    static int minlhs = 0;
    static int minrhs = 1;
    static int maxlhs = 2;
    static int maxrhs = 1;
    CheckRhs(minrhs, maxrhs);
    CheckLhs(minlhs, maxlhs);

    // Get A
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 1, piAddr, &lrA, &M, &N, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    if (N != M)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "A square matrix expected.\n"),
                 fname, 1);
        return 1;
    }

    // Create d
    sciErr = allocMatrixOfDouble(pvApiCtx, 2, N, 1, &lrd);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    // Create a workspace
    //WORK = 3;
    //LWORKMIN = Max(1, 3*N-1);
    //sciErr = allocMatrixOfDouble(pvApiCtx, 3, LWORK, 1, &lrWORK);
    //if (sciErr.iErr)
    //{
    //    printError(&sciErr, 0);
    //    return 1;
    //}
    LDA = Max(1, N);

    if (Lhs == 2)
    {
        double sz = 0;
        int vp = -1;
		/* [R d] = linalg_dsyev(A) */
        WORK = 3;
        LWORKMIN = Max(1, 3*N-1);
        C2F(dsyev)("V", "L", &N, lrA, &LDA, lrd, &sz, &vp, &INFO);
        LWORK = (int)sz;
        printf("\ntaille allouée: %d\n", LWORK);
        sciErr = allocMatrixOfDouble(pvApiCtx, 3, LWORK, 1, &lrWORK);

        C2F(dsyev)("V", "L", &N, lrA, &LDA, lrd, lrWORK, &LWORK, &INFO);
        if (INFO != 0)
        {
            char * fname = "dsyev";
            unsigned long fname_len = 5L;
            int i = 0;
            #define nlgh 24
            char Fname[nlgh+1];
            int minlength = Min(fname_len, nlgh);
            strncpy(Fname,fname, minlength);

            Fname[minlength] = '\0';

            for (i =  0;
                 i <  (int)minlength;
                 i += 1)
            {
                if (Fname[i] == ' ')
                {
                    Fname[i] = '\0';
                    break;
                }
            }
            Scierror(998, _("%s: internal error, info=%d.\n"),
                     Fname, INFO);
            return 1;

        }
        LhsVar(1) = 1;
        LhsVar(2) = 2;
    }
    else
    {
        /* d = linalg_dsyev(A) */
		double sz = 0;
        int vp = -1;
        WORK = 3;
        LWORKMIN = Max(1, 3*N-1);
        C2F(dsyev)("N", "L", &N, lrA, &LDA, lrd, &sz, &vp, &INFO);
        LWORK = (int)sz;
        printf("\ntaille allouée: %d\n", LWORK);
        sciErr = allocMatrixOfDouble(pvApiCtx, 3, LWORK, 1, &lrWORK);
        C2F(dsyev)("N", "L", &N, lrA, &LDA, lrd, lrWORK, &LWORK, &INFO);
        if (INFO != 0) {
            char* fname = "dsyev";
            unsigned long fname_len = 5L;
            int i = 0;
            #define nlgh 24
            char Fname[nlgh+1];
            int minlength = Min(fname_len, nlgh);
            strncpy(Fname,fname, minlength);
            Fname[minlength] = '\0';
            for (i =  0;
                 i <  (int)minlength;
                 i += 1)
            {
                if (Fname[i] == ' ')
                {
                    Fname[i] = '\0';
                    break;
                }
            }
            Scierror(998, _("%s: internal error, info=%d.\n"),
                     Fname, INFO);
            return 1;
        }
        LhsVar(1) = 2;
    }
    return 0;
}

