// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009-2010 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <limits.h>
#include "Scierror.h"
#include "localization.h"
#include "api_scilab.h"

#include "gw_linalg_support.h"

// Get a pointer to the doubles of a real matrix of doubles
// from input argument #ivar.
int getRealMatrixOfDoubles(
        char * fname,        int ivar,
         int * piAddr, double ** lrA,
         int * mA,         int * nA,
        void * pvApiCtx)
{
    int iRet = 0;
    int iType = 0;
    int iComplex = 0;
    SciErr sciErr;

    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (iType != sci_matrix)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d. "
                        "Matrix expected.\n"),
                 fname, ivar);
        return 1;
    }
    iComplex = isVarComplex(pvApiCtx, piAddr);
    if (iComplex == 1)
    {
        Scierror(999, _("%s: Wrong type for argument %d: "
                        "Real matrix expected.\n"),
                 fname, ivar);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddr, mA, nA, lrA);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    return 0;
}

// Get a pointer to the real and imaginary doubles of a complex matrix
// of doubles from input argument #ivar.
int getComplexMatrixOfDoubles(
           char * fname,        int ivar,
            int * piAddr, double ** lrA,
        double ** liA,        int * mA,
            int * nA,        void * pvApiCtx)
{
    int iRet = 0;
    int iType = 0;
    int iComplex = 0;
    SciErr sciErr;

    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (iType != sci_matrix)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d. "
                        "Matrix expected.\n"),
                 fname, ivar);
        return 1;
    }
    iComplex = isVarComplex(pvApiCtx, piAddr);
    if (iComplex == 0)
    {
        Scierror(999, _("%s: Wrong type for argument %d: "
                        "Complex matrix expected.\n"),
                 fname, ivar);
        return 1;
    }
    sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, mA, nA, lrA, liA);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    return 0;
}

