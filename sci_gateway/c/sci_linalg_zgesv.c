// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <stdlib.h>

// Headers from Scilab:
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"

// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/*********************************/
/*                               */
/*     X = linalg_zgesv(A,B)     */
/*                               */
/*********************************/

int sci_linalg_zgesv(GW_PARAMETERS)
{
    int MA = 0, NA = 0;
    int MB = 0, NB = 0;

    int * piAddr = NULL;
    SciErr sciErr;
    int iRet = 0;
    double * lrA = NULL, * liA = NULL;
    double * lrB = NULL, * liB = NULL;
    int * ipiv = NULL;

    int info = 0;
    doublecomplex * lA = NULL;
    doublecomplex * lB = NULL;

    double * lrX = NULL;
    double * liX = NULL;

    CheckRhs(2, 2);
    CheckLhs(0, 1);

    // Get A
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getComplexMatrixOfDoubles(fname, 1, piAddr, &lrA, &liA, &MA, &NA, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    if (NA != MA)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "A square matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    lA = oGetDoubleComplexFromPointer(lrA, liA, MA*NA);

    // Get B
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getComplexMatrixOfDoubles(fname, 2, piAddr, &lrB, &liB, &MB, &NB, pvApiCtx);
    if (iRet)
    {
        return 1;
    }
    if (MB != MA)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 2, MA, NB);
        return 1;
    }
    lB = oGetDoubleComplexFromPointer(lrB, liB, MB*NB);
    // Create array ipiv
    ipiv = (int*)malloc(MA * sizeof(int));
    if (ipiv == NULL)
    {
        Scierror(999, _("%s: No more memory.\n"), fname);
        return 1;
    }
    // Call zgesv
    C2F(zgesv)(&MB, &NB, lA, &MA, ipiv, lB, &MB, &info);
    if (info != 0)
    {
        Scierror(999, _("%s: Matrix is singular.\n"), fname);
        return 1;
    }
    // Free memory
    free(ipiv);
    // Create output variable: X
    sciErr = allocComplexMatrixOfDouble(pvApiCtx, Rhs+1, MB, NB, &lrX, &liX);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    // Transfert doublecomplex in B to real and imaginary parts in X.
    vGetPointerFromDoubleComplex(lB, MB*NB, lrX, liX);
    // Let Scilab know that Rhs + 1 is the 1st (and only) output argument
    LhsVar(1) = Rhs+1;
    // Free memory
    vFreeDoubleComplexFromPointer(lA);
    vFreeDoubleComplexFromPointer(lB);
    return 0;
}

