// Copyright (C) INRIA
// Copyright (C) 2010 - DIGITEO - Allan CORNET
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Headers from Scilab:
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"

// Headers from the gateway:
#include "gw_linalg.h"
#include "gw_linalg_support.h"

/**********************************************************/
/*                                                        */
/*     Usage:  C = linalg_dgemm(alfa, A, B, betha, C)     */
/*                                                        */
/**********************************************************/

int sci_linalg_dgemm(GW_PARAMETERS)
{
    int m = 0;
    int n = 0;
    int k = 0;
    int mA = 0;
    int nA = 0;
    int mB = 0;
    int nB = 0;
    int mC = 0;
    int nC = 0;

    int minlhs = 0;
    int minrhs = 5;
    int maxlhs = 1;
    int maxrhs = 5;

    int * piAddr = NULL;
    double alpha = 0;
    double beta  = 0;
    SciErr sciErr;
    int iRet = 0;
    double * lrA = NULL;
    double * lrB = NULL;
    double * lrC = NULL;
    int iType = 0;
    int iComplex = 0;

    CheckRhs(minrhs, maxrhs) ;
    CheckLhs(minlhs, maxlhs) ;

    // Get alpha (arg #1)
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = isVarComplex(pvApiCtx, piAddr);
    if(iRet)
    {
        Scierror(1001,_("%s: Wrong type for argument %d: "
                        "Real matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    iRet = getScalarDouble(pvApiCtx, piAddr, &alpha);
    if (iRet)
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 1, 1, 1);
        return 1;
    }

    // A (arg #2)
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 2, piAddr, &lrA, &mA, &nA, pvApiCtx);
    if (iRet)
    {
        return 1;
    }

    // B (arg #3)
    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 3, piAddr, &lrB, &mB, &nB, pvApiCtx);
    if (iRet)
    {
        return 1;
    }

    // Get beta (arg #4)
    sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        Scierror(999, _("%s: Wrong size for input argument #%d: "
                        "%d-by-%d matrix expected.\n"),
                 fname, 4, 1, 1);
        return 1;
    }
    iRet = isVarComplex(pvApiCtx, piAddr);
    if (iRet)
    {
        Scierror(999, _("%s: Wrong type for argument %d: "
                        "Real matrix expected.\n"),
                 fname, 4);
        return 1;
    }
    iRet = getScalarDouble(pvApiCtx, piAddr, &beta);
    if (iRet)
    {
        return 1;
    }

    // C (arg #5)
    sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddr);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    iRet = getRealMatrixOfDoubles(fname, 5, piAddr, &lrC, &mC, &nC, pvApiCtx);
    if (iRet)
    {
        return 1;
    }

    m = mA;
    n = nB;
    if (nA != mB
     || mA != mC
     || nB != nC )
    {
        Scierror(999, "%f: invalid matrix dims\n", fname);
        return 1;
    }

    k = nA;
    C2F(dgemm)(
            "n", "n", &m, &n, &k, &alpha,
            lrA, &mA, lrB, &mB, &beta, lrC, &mC);

    /*  Return C (#5) */
    LhsVar(1) = 5;
    return 0;

}

