// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// gw_linalg.h
//   Header for the LINALG gateway.

#ifndef __SCI_GW_LINALG_H__
#define __SCI_GW_LINALG_H__

#include "version.h"
#if SCI_VERSION_MAJOR < 6
#define GW_PARAMETERS char *fname, unsigned long fname_len
#else
#define GW_PARAMETERS char *fname, void *pvApiCtx
#endif

int sci_linalg_dgemm(GW_PARAMETERS);
int sci_linalg_dsyev(GW_PARAMETERS);
int sci_linalg_powfast(GW_PARAMETERS);
int sci_linalg_zgemm(GW_PARAMETERS);
int sci_linalg_zhbev(GW_PARAMETERS);
int sci_linalg_dgesv(GW_PARAMETERS);
int sci_linalg_zgesv(GW_PARAMETERS);

#endif /* __SCI_GW_LINALG_H__ */
